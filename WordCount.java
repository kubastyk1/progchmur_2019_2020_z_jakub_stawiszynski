package pl.com;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.json.JSONObject;

public class WordCount {

  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, DoubleWritable>{

    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString());
      while (itr.hasMoreTokens()) {
    	  
    	  JSONObject obj = new JSONObject(itr.nextToken());
          String side = obj.getString("side");
          int series = obj.getInt("series");

          JSONObject features = obj.getJSONObject("features2D");
          
          word.set(side + "-" + series + "-" + "first");
          context.write(word, new DoubleWritable(features.getDouble("first")));
          word.set(side + "-" + series + "-" + "second");
          context.write(word, new DoubleWritable(features.getDouble("second")));
          word.set(side + "-" + series + "-" + "third");
          context.write(word, new DoubleWritable(features.getDouble("third")));
          word.set(side + "-" + series + "-" + "fourth");
          context.write(word, new DoubleWritable(features.getDouble("fourth")));
          word.set(side + "-" + series + "-" + "fifth");
          context.write(word, new DoubleWritable(features.getDouble("fifth")));
          
      }
    }
  }

  public static class DoubleSumReducer
       extends Reducer<Text,DoubleWritable,Text,DoubleWritable> {
    private DoubleWritable result = new DoubleWritable();

    public void reduce(Text key, Iterable<DoubleWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
    	
      List<Double> valueList = new ArrayList<Double>();
      values.forEach(v -> valueList.add(v.get()));
      
      double sum = 0;
      int arraySize = 0;

      for (double val : valueList) {
        sum += val;
        arraySize++;
      }
      double average = sum / arraySize;

      double variation = 0;
      for (double val : valueList) {
    	  variation += Math.pow(val - average, 2);
	  }
      
      double stdDev = Math.sqrt(variation / arraySize);
      
      result.set(stdDev);
      context.write(key, result);
  
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "word count");
    job.setJarByClass(WordCount.class);
    job.setMapperClass(TokenizerMapper.class);
    //job.setCombinerClass(DoubleSumReducer.class);
    job.setReducerClass(DoubleSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(DoubleWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}